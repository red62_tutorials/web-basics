var car = {
   wheels: 4,
   color:'red',
   interior:'black',
   engine: 8,
   owners:[{name:'james', age:24 }, {name:'scott', age:20}],
   messageOwner: function(name){
      return 'sending a message to '+name
   }
}
console.log(car.messageOwner('bob'));